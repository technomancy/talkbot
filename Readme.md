# Talkbot

Just for kicks.

Uses [Luvit](http://luvit.io); install that and run `lit install` then `luvit .`.

## License

Code copyright © 2015 Phil, Noah, and Zach Hagelberg

Distributed under the GNU General Public License version 3; see file LICENSE.

