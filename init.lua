local lume = require("lume")
local irc = require("irc")

local server = process.env["IRC_HOST"] or "irc.freenode.net"
local channel = process.env["IRC_CHANNEL"] or "#talkbot"
local nick = process.env["IRC_NICK"] or "talkbot"
local conn = irc:new(server, nick, {auto_connect=true, auto_join={channel}})
local say = function(...) conn:say(channel, string.format(...)) end

local f = io.open("memory.lua")
memory = lume.deserialize(f:read("*a"))
f:close()

local respond = require("respond")

conn:on("message", function(from, to, original_text)
           if(not original_text:match("^" .. nick .. ":")) then return end
           local text = original_text:lower():match("^" .. nick .. ": (.+)$")
           if(text == "reload") then
              lume.hotswap("respond")
              say("Reloaded!")
           elseif(text == "save") then
              local f = io.open("memory.lua", "w")
              f:write(lume.serialize(memory))
              f:close()
              say("Saved memory.")
           else
              local ok, err = pcall(respond.respond, say, from, text)
              if(not ok) then print("Problem:", err) end
           end
end)

conn:on("connecting", function(_) print("Connecting... ") end)
conn:on("connect", function(_) print("done.") end)
conn:on("disconnect", function(reason) print("Disconnect:", reason) end)
conn:on("connecterror", function(reason) print("error:", reason) end)
