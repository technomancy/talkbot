return {
   -- functions
   type = type,
   pairs = pairs,
   ipairs = ipairs,
   next = next,
   unpack = unpack,
   tonumber = tonumber,
   tostring = tostring,

   -- tables
   coroutine = coroutine,
   math = math,
   table = table,
   string = string,
}
