local lume = require("lume")
local sandbox = require("sandbox")

local greetings = {"hello", "hi", "aloha", "mingalaba"}

return {
   respond = function(say, from, text)
      if(text:match("^say (.+)")) then
         say(text:match("^say (.+)"))
      elseif(text:match("lua: .+")) then
         local code = text:match("lua: (.+)")
         local chunk = assert(loadstring(code))
         sandbox.say = say
         setfenv(chunk, sandbox)
         say(tostring(chunk()))
      elseif(text:match("what is .+?")) then
         local word = text:match("what is (.+)?")
         if(memory[word]) then
            say(word .. " is " .. memory[word])
         else
            say("I don't know what " .. word .. " is.")
         end
      elseif(text:match("[a-z]+ is .+")) then
         local word, meaning = text:match("([a-z]+) is (.+)")
         memory[word] = meaning
         say("You got it.")
      elseif(lume.find(greetings, text)) then
         local greeting = greetings[math.random(#greetings)]
         say(greeting .. ", " .. from .. "!")
      else
         say("I don't know that.")
      end
   end
}

