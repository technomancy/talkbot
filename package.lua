return {
  name = "technomancy/irc-activity-bot",
  version = "0.1.0",
  description = "a bot for stuff",
  keywords = {"irc", "bot"},
  homepage = "https://gitlab.com/technomancy/luvit-irc-activity-bot",
  authors = {
     {name = "Ryan Liptak", url = "http://www.ryanliptak.com"},
     {name = "Phil Hagelberg", url = "http://technomancy.us"},
  },
  dependencies = {
    "luvit/luvit@2.1.1",
    "technomancy/irc@0.3.3",
  },
  files = {
    "!tests",
    "**.lua"
  }
}
